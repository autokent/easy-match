'use strict';

var matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;

const escapeStringRegexp = function (str) {
	if (typeof str !== 'string') {
		throw new TypeError('Expected a string');
	}

	return str.replace(matchOperatorsRe, '\\$&');
};

const reCache = new Map();

function makeRe(pattern) {
	const cacheKey = pattern;

	if (reCache.has(cacheKey)) {
		return reCache.get(cacheKey);
	}

	var re = /^\/((?:\\\/|[^\/])+)\/$/;

	if(re.test(pattern)){
		let str = re.exec( pattern );
		if(str)
		{
			const re1 = new RegExp(str[1], 'i');
			reCache.set(cacheKey,re1);
			return re1;
		}
		else{
			reCache.set(cacheKey,null);
			return null;
		}
	}
	else if(pattern.includes('*')){
		//debugger;
		pattern = escapeStringRegexp(pattern).replace(/\\\*/g, '.*');
		
		const re2 = new RegExp(`^${pattern}$`, 'i');
		reCache.set(cacheKey, re2);
		return re2;
	}
	else{
		//debugger;
		const re3 = new RegExp(`${pattern}`, 'i');
		reCache.set(cacheKey, re3);
		return re3;
	}

}

function EasyMatch(inputs, patterns,stat){
    if (typeof stat === 'undefined') stat = false;
	let ret = { matches:[],others:[],stat:[],ismatch:false};

	if(!Array.isArray(inputs)) inputs=[inputs];

	if (!(Array.isArray(inputs) && Array.isArray(patterns))) {
		throw new TypeError(`Expected two arrays, got ${typeof inputs} ${typeof patterns}`);
	}

	if (patterns.length === 0) {
		ret.yes= inputs;
		return ret;
	}

	let patternMap = new Map();

	for(let pattern of patterns)
	{
		let patternRegex = makeRe(pattern);
		patternMap.set(pattern,{"pattern":pattern,"regex":patternRegex,"matches":[]});
	}

	let isMatch = false;
	for (let input of inputs) {
		isMatch = false;

		for (var [key, value] of patternMap){
			if (value.regex != null && value.regex.test(input)) {
				isMatch = true;
				if(stat) value.matches.push(input);
				break;
			}
		}

		if (isMatch) {
			ret.matches.push(input);
		}
		else {
			ret.others.push(input);
		}
	}
	let patternMapArr =  Array.from(patternMap.values());
	if(stat) ret.stat = patternMapArr.filter(e=>e.matches.length>0);

	ret.ismatch = ret.matches.length > 0 ? true : false;

	return ret;
}

module.exports = EasyMatch;

//for testing purpose.
if (!module.parent) {
	console.log(EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'aaa\fff/bar.pdf', 'mooo','\nasdee'], ['it@*','*it@*', '*.pdf','*@*abc*','moo','/\\sasd/']));
}
