const assert = require('assert');
const EasyMatch = require('../');

describe('contains test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@'], ['it@']);
        assert.equal(res.ismatch,true);
        assert.equal(res.matches.length,4);
        assert.equal(res.matches.includes('it@abc.com'),true);
        assert.equal(res.matches.includes('itit@abc.com'),true);
        assert.equal(res.others.includes('xyz@abc.com'),true);
        assert.equal(res.matches.includes('abcit@domain.com'),true);
        assert.equal(res.matches.includes('abcit@'),true);
    });
});


describe('left wildcard test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@","itit@"], ['*it@']);
        assert.equal(res.ismatch,true);
        assert.equal(res.matches.length,3);
        assert.equal(res.others.includes('it@abc.com'),true);
        assert.equal(res.others.includes('itit@abc.com'),true);
        assert.equal(res.others.includes('xyz@abc.com'),true);
        assert.equal(res.others.includes('abcit@domain.com'),true);
        assert.equal(res.matches.includes('abcit@'),true);
        assert.equal(res.matches.includes('it@'),true);
        assert.equal(res.matches.includes('itit@'),true);
    });
});


describe('right wildcard test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['it@*']);
        assert.equal(res.ismatch,true);
        assert.equal(res.matches.length,2);
        assert.equal(res.matches.includes('it@abc.com'),true);
        assert.equal(res.others.includes('itit@abc.com'),true);
        assert.equal(res.others.includes('xyz@abc.com'),true);
        assert.equal(res.others.includes('abcit@domain.com'),true);
        assert.equal(res.others.includes('abcit@'),true);
        assert.equal(res.matches.includes('it@'),true);
    });
});


describe('inner wildcard test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['*it@*']);
        assert.equal(res.ismatch,true);
        assert.equal(res.matches.length,5);
        assert.equal(res.matches.includes('it@abc.com'),true);
        assert.equal(res.matches.includes('itit@abc.com'),true);
        assert.equal(res.others.includes('xyz@abc.com'),true);
        assert.equal(res.matches.includes('abcit@domain.com'),true);
        assert.equal(res.matches.includes('abcit@'),true);
        assert.equal(res.matches.includes('it@'),true);
    });
});


describe('full wildcard test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com','itit@abc.com','itxyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['*it*@*']);
        assert.equal(res.ismatch,true);
        assert.equal(res.matches.length,6);
        assert.equal(res.matches.includes('it@abc.com'),true);
        assert.equal(res.matches.includes('itit@abc.com'),true);
        assert.equal(res.matches.includes('itxyz@abc.com'),true);
        assert.equal(res.matches.includes('abcit@domain.com'),true);
        assert.equal(res.matches.includes('abcit@'),true);
        assert.equal(res.matches.includes('it@'),true);
    });
});


describe('regex test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['/^it@.*$/']);
        assert.equal(res.ismatch,true);
        assert.equal(res.matches.length,2);
        assert.equal(res.matches.includes('it@abc.com'),true);
        assert.equal(res.others.includes('itit@abc.com'),true);
        assert.equal(res.others.includes('xyz@abc.com'),true);
        assert.equal(res.others.includes('abcit@domain.com'),true);
        assert.equal(res.others.includes('abcit@'),true);
        assert.equal(res.matches.includes('it@'),true);
    });
});


describe('non-match test', function() {
    it('should match', function() {
        let res = EasyMatch(['it@abc.com'], ['xyz']);
        assert.equal(res.ismatch,false);
    });
});
