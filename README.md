# easy-match
> **A simple matcher with regex cache, zero dependency.**

![logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/4802193/easy-match.png)

[![version](https://img.shields.io/npm/v/easy-match.svg)](https://www.npmjs.org/package/easy-match)
[![downloads](https://img.shields.io/npm/dt/easy-match.svg)](https://www.npmjs.org/package/easy-match)
[![node](https://img.shields.io/node/v/easy-match.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/easy-match/badges/master/pipeline.svg)](https://gitlab.com/autokent/easy-match/pipelines)

## Installation
`npm install easy-match`

## Usage

### Contains
```js
const EasyMatch = require('easy-match');

let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@'], ['it@']);
console.log(res.matches.length);//4
console.log(res.matches.includes('it@abc.com'));//true
console.log(res.matches.includes('itit@abc.com'));//true
console.log(res.others.includes('xyz@abc.com'));//true
console.log(res.matches.includes('abcit@domain.com'));//true
console.log(res.matches.includes('abcit@'));//true
```

### Left Wildcard
```js
const EasyMatch = require('easy-match');

let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@","itit@"], ['*it@']);
console.log(res.matches.length);//3
console.log(res.others.includes('it@abc.com'));//true
console.log(res.others.includes('itit@abc.com'));//true
console.log(res.others.includes('xyz@abc.com'));//true
console.log(res.others.includes('abcit@domain.com'));//true
console.log(res.matches.includes('abcit@'));//true
console.log(res.matches.includes('it@'));//true
console.log(res.matches.includes('itit@'));//true
```

### Right Wildcard
```js
const EasyMatch = require('easy-match');

let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['it@*']);
console.log(res.matches.length);//2
console.log(res.matches.includes('it@abc.com'));//true
console.log(res.others.includes('itit@abc.com'));//true
console.log(res.others.includes('xyz@abc.com'));//true
console.log(res.others.includes('abcit@domain.com'));//true
console.log(res.others.includes('abcit@'));//true
console.log(res.matches.includes('it@'));//true
```

### Inner Wildcard
```js
const EasyMatch = require('easy-match');

let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['*it@*']);
console.log(res.matches.length);//5
console.log(res.matches.includes('it@abc.com'));//true
console.log(res.matches.includes('itit@abc.com'));//true
console.log(res.others.includes('xyz@abc.com'));//true
console.log(res.matches.includes('abcit@domain.com'));//true
console.log(res.matches.includes('abcit@'));//true
console.log(res.matches.includes('it@'));//true
```

### Full Wildcard
```js
const EasyMatch = require('easy-match');

let res = EasyMatch(['it@abc.com','itit@abc.com','itxyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['*it*@*']);
console.log(res.matches.length);//6
console.log(res.matches.includes('it@abc.com'));//true
console.log(res.matches.includes('itit@abc.com'));//true
console.log(res.matches.includes('itxyz@abc.com'));//true
console.log(res.matches.includes('abcit@domain.com'));//true
console.log(res.matches.includes('abcit@'));//true
console.log(res.matches.includes('it@'));//true
```

### Regex
```js
const EasyMatch = require('easy-match');

let res = EasyMatch(['it@abc.com','itit@abc.com','xyz@abc.com', 'abcit@domain.com','abcit@',"it@"], ['/^it@.*$/']);
console.log(res.matches.length);//2
console.log(res.matches.includes('it@abc.com'));//true
console.log(res.others.includes('itit@abc.com'));//true
console.log(res.others.includes('xyz@abc.com'));//true
console.log(res.others.includes('abcit@domain.com'));//true
console.log(res.others.includes('abcit@'));//true
console.log(res.matches.includes('it@'));//true
```

## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.